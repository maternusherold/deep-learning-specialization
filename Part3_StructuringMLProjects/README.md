# Structuring Machine Learning Projects

TODO

## Week1

important take-aways are:

- *orthogonalization refers to assigning one task/effect to one control unit. Such as a car has a steering wheel for steering, break for reducing the speed and a paddle for acceleration. In such a way one can precisely tune the cars driving instead of having a nob which changes these artifacts as a function like $`0.8*speed + 0.1*break - 0.5*acceleration`$
- the term orthogonal describes the idea coming from a coordinate system whith 90° axis. If you change on axis, the other axis does not change.
- e.g. *early-stopping* is effecting the training and is regularizingand therefore not orthogonal
- *single number eval. metric* describes a metric combining several metrics one is interested in, e.g. F-score. This helps to compare models with different performances
- one can also split desired metrics into *optimizing* and *satisficing* metrics where the optimizing are trying to be optimized and the others just have to be satisfied
- *avoidable bias* describes the difference between current training error and (optimal) Bayes error, i.e. the lowest error possible while human performance usually is larger than Bayes error. However, human performance usually is a proxy for Bayes error rate
- to reduce the avoidable bias: train bigger model, train longer, use better optimization alg, change model architecture (if applicable)
- to reduce variance: get more data, regularization (L2, Dropout, *data augmentation*) or change network/model architecutre
- even when it is desriable to have the same distribution in training, validation and test data it can help to add new data to the train set even though it is not represented in the validation/test set as it can help the model to generalize and learn features

## Week 2

important take-aways are:

- when trying to improve models, make sure to investigate which areas hold the highest return on investment as highest improvement for the put in work. check the errors the model made and compare the amount of error the different parts contribute to the overall error
- usually, neural network can cope with random label errors - if the overall data is big enough - but will be negatively influenced by systematic errors
- in case there are several data sources while only one represents the final target distribution the additional data sources can be mixed into training data as it might help to learn features but only the target data should be included into the validation and test data (last bullet from week 1); if the target data is big enough, some could also be included into training but none of the non-target data into val./test data
- added data from different distributions than the target distribution can lead to *data mismatch* problems 
- *transfer learning* for transfering from A to B makes sense, if task A and B have the same input *x* or there is a lot more data for task A than for task B or low level features from task A can be helpful for task B (logic *or* connections here and a *and* connection even fosters transfer learning)
- *multi-task learning* tries to fit one model to several tasks; e.g. one model to detect stop signs, cars, pedestrians, ... This might be preferable over fitting multiple models as common lower-level features in upper layers are shared, i.e. edge detection etc. this is especially useful when there is little data for each task but together they have a lot of data and help each other with lower-level features. However, multi-task learning can fail when the network is not *'big'* enough 
- *end-to-end deep learning* can make many intermediate steps in a pipeline obsolete, i.e. skip prev. neccessary steps to transform features and map inputs to outputs directly. End-to-end learing works well when there is enough data labeled directly as $`(input, output)`$ but often there is only data to solve intermediate steps why the pipeline again is split up again
- a benefit of end-to-end learning is that there is less hand-designed features/steps and the model can learn from the model, i.e. not using human given features which might not be helpful 
- a drawback is that (maybe) useful hand-designed influence is hard to incorporate as the the only influence comes from the data, e.g. self-driving is better solved without end-to-end learning 

