# Convolutional Neural Networks

*all sketches are taken from Andrew Ng's slides*

TODO

## Week1 - Foundations of CNNs

important take-aways are:

- convolution is (very simple) defined as the sum over an element-wise multiplication between an kernel/filter and a same-size region in the image; running this filter over the whole image defines the complete convolution and thereby computes the different pixels, i.e. cross-correlation
- for edge detection a filter is used which computes rel. high values on the one side, zeros in the middle and rel. low values on the other side, e.g. `[[1,1,1], [0,0,0], [-1,-1,-1]]` would detect a vertical edge
- more robust vertical edge detection filters where introduced with the *Sobel* and *Scharr* filter which put more emphasize on the middle row values. However, it's usually best to **learn** the filters values as weights during training
- padding works against shrinking the output image and throwing away information from the image edges (thrown away as those pixels are less often included in convolutions as inner pixels); the output dimension (using stride 1) on a $`n \times n`$ image with a kernel of size $`k \times k`$ is computed by: $`(n + 2*p - k + 1) \times (n + 2*p - k + 1)`$ where $`p`$ describes the padding size; without padding less pixels in the next layer depend on the edge-pixels of the prev. filter
- two common choices of padding: valid (no padding, i.e. $`n \times n, k \times k`$) and same (pad the input s.t. the output is the same size i.e. $` n + 2*p - k + 1 = n \to p = \frac{k - 1}{2}`$)
- kernel size is usually odd, due to above same filter yielding an int padding and as it might be nice to have a *center* of the filter
- incorporating stride of size $`s`$ as well, the output dimensions are computed by: $`\lfloor \frac{n + 2*p - k}{s} + 1 \rfloor \times \lfloor \frac{n + 2*p - k}{s} + 1 \rfloor`$
- for a volumetric image, e.g. RGB, the filter must be of the same dimension as well, i.e. 3D. The dimension of filter is inherited of the number of features output from the prev. layer. the dimension of a conv layer often is described as $`n^{[l]}_W \times n^{[l]}_H \times n^{[l-1]}_C \times n^{[l]}_C`$
- pooling operations happen on each filter dimension before aggregation

## Week2 - Deep CNNs

introduction to classic and effective convolutional neural networks to show examples and investigate how they work. Some of the presented architectures are LeNet-5, AlexNet, VGG, ResNet and the Inception network.

important take-aways are:

- LeNet-5 followed a Conv-Pooling style by applying two Conv-Pooling blocks followed by two fully connected layers and an output layer which had 10 units (MNIST) but was not using softmax back then. Also, it used average pooling with the activation function being applied after the pooling instead of after the Conv part. Also, it mostly used sigmoid (maybe tanh) activation functions and only applied Conv filters to certain input channels from before to save on computation. It did not use any *same* padding which lead to a steady increase of image size. Most increase happened due to the pooling layers. LeNet-5 was published in 1998 and has roughly 60k parameters.

![LeNet architecture](./readme_images/lenet.png "LeNet architecture")

- AlexNet was published in 2012 and named after Alex Krizhevsky (first author).	Again it mostly consists of Conv-Pooling blocks but now uses max pooling and some *same* padding convolutions. Also, the filter size was held at $`3 \times 3`$ for a couple of operations while the depth changed. It ended with some fully connected layers and an softmax layer of 1000 units (ImageNet). It was now much bigger than LeNet-5 with 60M parameters. Also, in contrast to LeNet it used the ReLU activation function. With its success it demonstrated the impact and performance deep learning can have in computer vision. A special feature of AlexNet is *Local Response Normalization*, normalizing the values of one feature element across all channels, which isn't used today anymore. (it is a more easy DL paper for CV to start reading)

![AlexNet architecture](./readme_images/alexnet.png "AlexNet architecture")

- VGG-16 was published in 2015 and followed an approach to simplify operations and used a fixed set of convolution and pooling operations. Also, instead of running huge kernels it stuck to smaller ones on both conv. and pooling. It uses convolutions of $`3 \times 3`$ with a stride of one and *same* padding. For (only) max pooling operations it uses $`2 \times 2`$ kernels with a stride of 2. In contrast to the prev. models it now used Conv-Pooling blocks with more than one convolution (two or three) but keeping the hight and with fixed between those and halfing after each pooling step making it possible to train a deeper architecture. Also, the channel size doubled in each of the first conv. steps from 64 up to 512. Finally, a softmax with 1000 outputs regressed an image's category. It uses 138M parameters. The 16 in the name refers to the 16 layers having weights.

![VGG-16 architecture](./readme_images/vgg16.png "VGG-16 architecture")

- ResNet, published in 2015 as well, uses skip connections to mitigate the impact of vanishing gradients and therefore, train deeper architectures. A skip connection takes the activation of a prev. layer and adds (addition operation) to the linear output of a later layer which is then fed into activation function. Therefore, the features can have an direct impact even later in the layer and also get updated when the deeper layer is updated.
- the single blocks follow the order: *Conv -> Batch -> Activation -> Pooling*; the blocks themselves are used inside the identity or convolutional blocks. (see notebook)

![ResNet vs. Plain Architecture](./readme_images/resnet_plain.png "Coparing ResNet to plain architecture")

- Why do skip-connections work? first, to show that they do not hurt the model's ability in any kind, one can show that the identity function is as easy to learn as without the skip-connection. This is done via computing the activation of the deeper layer (incl. the skip-connection)  while using weight-decay and assuming a zero weight: $`g^{[l+2]}(W^{[l+2]}z^{[l+2]} + b^{[l+2]} + a^{[l]}) = g^{[l+2]}(a^{[l]}) = a^{[l]}`$. Then it is to show that the skip-connection even benefit the model. But that is not shown.
- for skip-connections to work the earlier activation has to be the same size as the linear component of the current layer. For this reason many blocks, using skip-connections, use same padding. Otherwise, one could multiply a matrix to the earlier activation added to adapt the size yielding: $`g^{[l+2]}(\dots + W_{size}*a^{[l]})`$
- while a $`1 \times 1`$ convolution might not be useful for a single channel it adds value when applied to multiple channels as it applies the inner product to a slice through the feature volume. It is also called *Network in network* and was published in 2013. It can be thought of as having a bunch of single neurons, being fully connected to the different slices in the prev. activation volume, i.e. building a MLP inside the model. Such a layer can either add just another layer of non-linearity or shrink the number of channels (see Inception net.).

![Grafic displaying 1-by-1 conv.](./readme_images/1-by-1_conv.png "1-by-1 Convolution")

- 1-by-1 conv. layers can also be used to reduce computation (see Inception)
- Inception net, published 2014, builds on the idea of inception modules which stack together different kinds of operations into a single volume, e.g. a volume consisting of $`1 \times 1,\ 3 \times 3,\ 5 \times 5, \dots`$ and pooling outputs (see below). The Inception is then a lot of these block as well as some side-branches to make predictions as well.

![Grafic displaying the inception module](./readme_images/inception_module.png "Inception module")

- tips for image augmentation: besides rotation, mirroring, cropping, etc. one can shift colors by adding brightness values to simulate different light settings; advanced way to utilize this is PCA color augmentation changing a lot of popular colors and applying only little changes to less popular colors, e.g. for a mainly purple image, the algorithm would mostly change R and B channels and less the G channel (see AlexNet paper).
- Statement: there are two sources of knowledge - labeled data and hand engineering (features, model architecture, ...) to incorporate human knowledge about the problem
- tips for doing well on benchmarks: ensemble learning and use some vote technique, multi-crop at test time (running multiple crops of the same image when testing)

![Tips for better performance on benchmarks](./readme_images/benchmark_tips.png "Tips for performing better on benchmarks")

## Week3 - Object detection

covers topics of not only classifying an image/objects in the image but also localize these objects. This includes one object but also multiple objects. An example output could be to draw bounding boxes around dogs, cats, cars, traffic lights etc. (this includes detection)

![Graphical difference between detection and localization](./readme_images/localization_example.png)

important take-aways:

- for detection one might output a softmax layer with a unit for each category, e.g. pedestrian, car, motorcycle, background. To include the localization one will include output units for the bounding box as well. Actually, there is even one more unit indicating the probability that there is an object present (all for the case of max. one object in the image)
- defining the target labels `y`: $`[p_o, b_x, b_y, b_w, b_h, c_1, \dots, c_n]`$ where the first unit describes the prob. of an object being in the image, the following describing the position and size of the bounding box and the last units describe detected object; in the case there is no object present the labels for bounding boxes etc. are *no care* labels

![Detection with localization setup](./readme_images/detection_with_localization.png "Detection with localization")

- for the first labels incl. the prob. of an object and its localization one could use a simple squared-error loss and a log-likelihood loss, i.e. cross-entropy loss, for the object describing labels
- landmark detection is used to find key points in an image. those can be the edges of the mouth, eyes etc. for faces or joints and other keypoints for pose estimation. Besides the coordinates for the landmarks, there's always a unit for the prob. of an object being present or not! Also, landmark detection is an important building block for AR and filters such as the ones provided by snapchat. Note, detection tasks include the localization task as described in the above illustration

![Facial keypoints detection and localization](./readme_images/facial_landmarks.png "Facial landmarks example")

- a simple but common approach to detection problems is the sliding windows algorithm in which windows of different sizes will be sled across an image (size by size) with a given stride, which should be rather small. The windows are fed to a network which was previously trained on cropped instances of the objects it should detect. So only images of just the object, e.g. a car or just a cat, were used to train the model. The overall pipeline then detects whenever such a slice/window is detected as an object. A down side, however, is the computational cost as it requires many different window sizes and small strides yielding many necessary predictions per image.
- a first step for the implementation of the sliding windows algorithm is the conversion of fully connected layers into $`1 \times 1`$ convolutional layers, i.e. applying the same number of suitable filters to a layer which it would had units when just flattening it. E.g. taking a $`5 \times 5 \times 16`$ conv. layer one would apply 400 filters of the size $`5 \times 5 \times 16`$ to receive an output of the shape $`1 \times 1 \times 400`$. with such a technique the layer is not just flattened but also yields trainable wights.
- in second step the whole image is just appended to the front of the model to act as input and to skip the sampling step as the cropping and sliding a window etc. basically is a convolutional operation. In such a fashion the whole process is parallelized
- to improve the bounding box predictions the YOLO (you only look once) algorithm puts a grid on top of the image and runs the sliding windows (convolution) algorithm on the grid cells. for each of such cells the algorithm then outputs a vector as described above for detection tasks while it assigns an object to the cell in which the center is located.

![YOLO grid setup](./readme_images/yolo_cells.png "YOLO grid")

- an metric for localization accuracy is the intersection over union (IoU) which computes the ration of prediction overlapping the correct bounding box and the overall region of both, the correct and predicted, bounding box. usually, an IoU of greater than 50% is counted as correct localization but can be tweaked for more "accuracy" - IoU is a measure for overlapping bounding boxes

```math
IoU = \frac{size overlapping bounding boxes}{size of both bounding boxes}
```

- non-max suppression helps to penalize multiple detections of the same object. the necessity for it is that with a fine grid many cells could detect an object as well as claim the objects center for this cell. this results in multiple detections of the same object, i.e. several bounding boxes. all bounding boxes are quantified with a probability value. *non-max suppression* takes the bounding box with the highest probability of an object and suppresses all other bounding box with a large enough IoU (e.g. IoU >.5). then it takes the next highest probability of an object and, again, suppresses all bounding boxes with a high IoU. This leads to removing the several overlapping bounding boxes in the picture below yielding one single bounding box per object

![Non-max suppression](./readme_images/non_max_suppression.png "Non-max suppression")

- another enhancement are *anchor boxes* which are predefined shapes for bounding boxes which probably fit the objects to be detected, i.e. a vertically stretched rectangle for pedestrians and a horizontally stretched rectangle for cars. for each cell the output vector has now increased to a multiple of the number of bounding boxes used, e.g. when there is a $`3 \times 3`$ grid, 2 anchors and each bounding box is described by a vector of eight dimensions the output is of shape $`3 \times 3 \times 2 \times 8`$ as there will be an output per cell; this can help when multiple objects have their center in the same cell and to detect objects of different shapes

![Anchor boxes](./readme_images/anchor_boxes.png "Anchor boxes")

- to conclude, the steps of the [YOLO-algorithm](https://arxiv.org/abs/1506.02640) basically are:
	1. define grid for which each cell will be searched for objects
	2. use anchor boxes to detect different kinds of objects and their different shapes
	3. remove all bounding boxes with little probability of objects (all cells will have  some bounding boxes)
	4. apply non-max suppression per object class, per cell, i.e. apply non-max suppression for cars on all cells and then for pedestrians ...

- region-proposal algorithms first try to identify regions for which it is useful to run an detection algorithm on. this is done via segmentation yielding blobs of different colors and identifying regions to run the detection on; a popular model from that class is the R-CNN (region with CNN) which is the predecessor of the Fast R-CNN as the initial model was rather slow. the difference between both are that R-CNN classifies each proposed region one at a time with the sliding windows algorithm, while the Fast R-CNN uses the convolutional approach to the sliding windows algorithm. still, with Fast R-CNN the proposal, i.e. segmentation step, was rather slow. The update, Faster R-CNN, uses CNNs to provide proposal regions. BUT STILL, even though the \*R-CNN family models yield a better detection performance but are slower than YOLO-based models

![R-CNN intuition](./readme_images/rcnn_intuition.png "R-CNN intuition")

## Week4 - Face detection and neural style transfer

covering face verification and face recognition in the first part. the face verification is a difficult propblem as it's a one-shot (1:1 learing) problem and also a building block for face recognition systems.

in general, face verification tries to solve a 1:1 problem identifiying the current image with a person's face, e.g. customs at the airport where a machine compares the face on the passport to the person's face infornt of the machine or when trying on unlock a phone using only the face.
on the opporsite, face recognition solves a 1:K problem where a face is compared to $`K`$ possibilities, e.g. at entry gates where $`K`$ constitutes the overall amount of allowed people entering

important take-aways:

### face detection:

- one-shot learning: learn from one sample (image)
- to keep one model while fitting a one-shot model a similarity function is used to measure the degree of difference between a proposes sample (image of a face) and any samples in e.g. a data base. If the degree of similary is below a threshold $`d(img_1,\ img_2) \leq \tau`$, the samples are close enough to match; otherwise, if $`d(img_1,\ img_2) > \tau`$, then they differ to much and the images will be rejected.
- a popular architecture is the Siamese Network which learns an encoding of an input in a number of outbuts, e.g. 128 output neurons make a 128 encoding. The samples are then compared by the norm between the two encodings. While similar images should result in a small norm, different images must result in a large value of the norm marking the difference; see [DeepFace](https://www.cs.toronto.edu/~ranzato/publications/taigman_cvpr14.pdf). This also yields how training is done

```math
d(x^{(i)},\ x^{(i+1)}) = \mid\mid f(x^{(i)}),\ f(x^{(i+1)}) \mid\mid_2
```

![Siamese Network](./readme_images/siamese_network.png "Siamese Network")

- a common loss for training face recognition models, i.e. siamese nets, is the *triplet loss* which tries to maximize the difference detected beteen an anchor image, a positive sample and an negative sample. to prevent the model from learning a trivial function, i.e. computing a difference of zero for all images, a margin term is added which itself is a hyperparameter and can be used to tweak the amount of differentiation of models. so given three images, the loss is defined as:

```math
L(I_A, I_P, I_N) = \operatorname{max} ( \mid\mid f(I_A) - f(I_P) \mid\mid^2 - \mid\mid f(I_A) - f(I_N) \mid\mid^2 + \alpha, 0)
```

- the model then tries to maximize the difference which leads to computing a maximal difference between different images. also, this indicates that one needs a data set with samples of the same person multiple times.
- it's important to not just use random pairs of positive and negative images as this leads to picking very dissimilar images and making it too easy for the model, i.e. $`d(I_A, I_P) + \alpha \leq d(I_A, I_N)`$ is easily satisfied. therefore, choose triplets which are close to each other and hard to seperate, i.e. $`d(I_A, I_P) \sim d(I_A, I_N)`$ whereby the model has to do *some work* to push the images (the norm's values) appart; see [FaceNet](https://arxiv.org/pdf/1503.03832.pdf)

![Triplet Loss data set](./readme_images/triplet_loss.png "Triplet Loss dataset")

- an alternative to the triplet loss is to treat the whole classification as a logistic regression problem is it might seem familiar to. this is achieved by computing the *n* dimensional encoding for two images and feeding an affine combination of both into a logist regression layer, i.e. a sigmoid node. With such an approach supervised learning is possible as the outputs are just `0/1` and can easily be backpropagated through the network. With this approach, the comparison itself is easier to train/adapt; see again [DeepFace](https://www.cs.toronto.edu/~ranzato/publications/taigman_cvpr14.pdf) for the elaboration on this

![Face recognition as binary classification](./readme_images/face_recog_binary.png "Face recognition as binary classification")

- to increase the accuracy in facial recognition one can compare an input image to multiple images of the same person and compute a score from each comparision and crop the background behind to make the algroithm more robust as it won't be distracted by irrelevant pixels

### neural style transfer:

- initially proposed by [Leon Gatys et al.](https://arxiv.org/abs/1508.06576)
- applies the style of a *style*-image to a input image where the style corresponds to the features learned by a CNN
- to imagine what kind of features a CNN detects, remeber to visualize inputs which result in high activation of the unit; e.g. pick some number of units per layer and find inputs which result in a high activation of those - this will also show the different levels of features which are detected by the layers (low-level features in the beginning, high level features at the end)
- below, input images and shapes are shown (nine inputs per unit) which result in high activation of nine units per layer

![Inputs which result in high unit actuvation](./readme_images/layer_viz.png "Inputs resulting in high activation per unit")

- to train a model the cost function as to reflect the task at hand, i.e. produce an image close to the input image in the style of the *style*-image, leading to two parts of the cost function. one between the generated image and the input and one between the generated image and the style image $`\to`$ the output of the model is a complete image

```math
J(G) = \alpha * J(G,I) + \beta * J(G,S)
```

- Content cost component is usually computed somewhere in the middle of  the network where the produced image still shows clear features of the input image; otherwise one would work against applying any style; the cost is then computed by e.g. the difference of the activations produced by both the input image and the generated image: $`J_{content}(G,I) = .5 ||a^{[l](I)} - a^{[l](G)}||_2`$
- to define a style of an image the correlation (or covariance) between activations of the same unit in the different channels can be computed, e.g. how often do vertical structures occur in combination with circles; this information is gathered in a *style-matrix*, i.e. a square matrix in the size of channels - which is actually the Gram matrix
- letting $`G`$ define the Gram matrix, then the cost function for the style part is defined as the Forbenius norm of the *style*-image and the generated image:

```math
J_{style} (G,S) = \frac{1}{2*n_H^{[l]}*n_W^{[l]}*n_C^{[l]})^2} \sum_k \sum_{k'} (G_{kk'}^{[l](G)} - G_{kk'}^{[l](S)})^2
```

- the cost functions are defined per layer as indicated by the superscript $`[l]`$ and the overall cost function can be computed as an weighted average of many of those

