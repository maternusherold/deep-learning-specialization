# Improving Deep Neural Networks: Hyperparameter tuning, Regularization and Optimization

part two of the deep learning specialization by deeplearning.ai contains three weeks of lectures.
The first week touches on practical aspects of deep learning as regularization, weight initialization as well as gradient checking.

## Week1

important take-aways are:

- initialization of weights is important to break symmetry which refers to different development in weights over training. If all weights are initialized to the same value, the symmetry would not be broken and the network would not learn; i.e. could be rewritten as linear transformation. Therefore, random initialization is important. 
- Also, initialize the weights to small values as otherwise only parts of the network with great weights would be updated. Usually us $`W^{[l]}=[-1,1]^{l \times (l-1)},\ b^[l] = 0`$
- regularization helps to reduce overfitting as it limits the weights in their sizes. This can be done by actually applying a regularization term on the costfunction or by utelizing dropout; i.e. randomly shutting off units per layer. 
- for simple regularization the common L2 norm is used which is called Frobenius norm as it is now applied on a matrix instead of a vector. 
- as regularization leads to a weight decay it is often referred as such; i.e. *weight-decay*
- for dropout it is important to keep in mind that the expected value per activation is changed when just switching off units. Therefore, the activation is divided by the $`1-dropout prop.`$ which corrects the expected value, called *inverted dropout*
- Gradient Checking can be used to double check the implemented backpropagation algorithm by computing a gradient estimate using the symmetric difference formula:

```math
\frac{J}{\theta} = \lim_{\epsilon \to 0} \frac{J(\theta + \epsilon) - J(\theta - \epsilon}{2\epsilon}
```

- gradient checking is costly, therefore, run it only sometimes during training to ensure backprop is working and turn it off afterwards

## Week 2

prerequisites:

- exponentially moving average: incl. prev. measured values (like a sliding window) to calculate an average. while the prev. moving-average-val. is included with a discount factor. if $`\beta`$ is the discount factor, then the moving average at time $`t`$ is given by

```math
v_t = \beta * v_{t-1} + (1-\beta) * x_t
```

- as a rule of thumb $`\frac{1}{1-\beta}`$ (exp. decay) describes the size of the window; i.e. greater window, smoother average
- to improve the estimates of the first values one uses bias correction by deviding the computed $`v_t`$ by $`1-\beta^t`$ which goes to zero for large $`t`$, which is why the bias corrected version is very similar to the inital formula above after the first values for which it yields better values

important take-aways are:

- mini-batch GD corresponds to a GD variation which processes only a part of the entire dataset until the first update step is done; i.e. this enables shorter training/feedback cycles
- with mini-batch GD the loss will not go down steadily but more in a zick-zack (noisy) as the mini-batches are different then the preceeding and following one
- stochastic GD is when `minibatch_size=1`; i.e. each example is its own mini-batch. Besides very noisy training it looses the advantages of vectorization used in mini-batch GD or batch GD
- to speed-up training, tweak mini-batch size so the batch fits into CPU-/GPU-memory
- momentum is a form of GD which computes an exponentially weighted avg. of the gradients and then uses that accumulated gradient for backpropagation, smoothing the steps of GD

```math
v_{dW_t} = \beta v_{dW_{t-1}} + (1-\beta) * dW
W_t = W_{t-1}-\alpha*v_{dW_t}
```

- RMSprop = root-mean-squared prop which also tries to stabalize learning (GD steps) by reducing ocilation such that a higher learning rate can be chosen. the moving average is computed with an element-wise squaring of the gradient.

```math
s_{dW_t} = \beta s_{dW_{t-1}} + (1-\beta) * dW^2
W_t = W_{t-1} - \alpha \frac{dW}{\sqrt{S_{dW_t}} + \epsilon
```

- RMSprop and Adam have proven to generalize well to a variety of problems instead of just special cases; Adam is a combination of RMSprop with momentum by computing a RMSprop like update but with a momentum-like moving average in the numerator

```math
v_{dW_t} = \beta_1 v_{dW_{t-1}} + (1-\beta_1) * dW    // momentum
s_{dW_t} = \beta_2 s_{dW_{t-1}} + (1-\beta_2) * dW^2  // RMSprop
v^{corrected}_{dW_t} = \frac{v_{dW_t}}{(1-\beta_1^t)}
s^{corrected}_{dW_t} = \frac{s_{dW_t}}{(1-\beta_2^t)}
W_t = W_{t-1} - \alpha \frac{v^{corrected}_{dW_t}}{\sqrt{s^{corrected}_{dW_t}}+\epsilon}
```

- a commmon choice for $`\beta_1,\beta_2`$ are $`.9,.999`$ as well as $`\epsilon=10^{-8}`$; usually only the hyperparameter $`\alpha`$ is tuned and the other three are left with the stated configuration
- the name originates from *adaptive moment estimation* as it computes estimates for the first (momentum) and second (RMSprop) moments
- learning rate decay can help to ocilate more closely around a minimum as the steps get smaller with increasing epochs. based on the `decay_rate` the inital learning rate is reduced per epoch, mini-batch or similar; common formulas are (e are epochs and t are mini-batches)

```math
\alpha_e = \frac{1}{1 + decay\_rate * |epoch|} * \alpha_0
\alpha_e = .95^{|epoch|} * \alpha_0 		// exp. decay
\alpha_e = \frac{c}{\sqrt{|epoch|}} * \alpha_0 
\alpha_t = \frac{c}{\sqrt{t}} * \alpha_0
```

## Week 3

important take-aways are:

- often sampling on a uniform scale is worse than sampling from a scale-adapted range; e.g. sampling uniformly from `[.001, 1.0]` then 90% of values are drawn from the resiong `[.1, 1.]` but usually one would like to have uniform increase in values to experience the effect of changes in magnitude. Therefore, sapmling on a the logarithmic transformation of the scale would make more sense as the numbers are on the same scale, i.e. sampling from `[-4,0]` as $`10^{-4} = .0001`$ and $`10^0 = 1`$. The same holds for various other ranges and parameters. When sampling e.g. the moving average a small change can have a very different effect on when it is applied. Using the formula to compute the included past examples a change of `.0005` can either result in a change of .05 or 1000 samples for $`\beta + .0005 = .9 + .0005 = .9005 \to 10`$ and $`\beta + .0005 = .999 + .0005 = .9995 \to 2000`$
- e.g. for sampling from $`[.9, .99]`$ on would compute the logarithm of base 10 for both ends, i.e. $`-.105, -.01`$. Then one can sample uniformly on a range which will then be added into a power function of base 10, i.e. $`beta = 1-10^{-r-1} \in [.01, .1]`$
```python
r = np.random.rand()
beta = 1 - 10**(-r - 1)
```
- the important take-away: based on the scale the parameter space can be explored more efficiently with the right scale
- for models in production and changing data one might want to re-tune the hyperparameters every few weeks to make sure the parameters are still suitable 
- batch norm stands for standardizing the outputs of the single unitsto speed up and stabalize training
- more often the normalizing function of $`z^{(i)}_{norm} = \frac{z^{(i)} - \mu}{\sqrt{\sigma^2 + \epsilon}}`$ is used ($`\epsilon`$ is used for num. stability). Also, variance and mean can be adjusted by learnable parameters $`\gamma, \beta`$ when computing $`\tilde{z}^{(i)} = \gamma * z^{(i)}_{norm} + \beta`$. Those changed z values are then fed to the activation functions. 
- using batch normalization leads to an elimination of the $`b^{[l]}`$ as it will be removed by the mean correction. This linear term is then replaced by the later addition of $`\beta^{[l]}`$ added to the normalized unit's output.
- batch norm. is computed per mini-batch and per layer it is used in, i.e. $`\mu, \sigma`$ are computed on the data from a mini-batch. during testing these standardization params. are estimated using weighted average across mini-batches, which is robust 
- during backpropagation also the batch norm. parameters $`\gamma, \beta`$ are updated

