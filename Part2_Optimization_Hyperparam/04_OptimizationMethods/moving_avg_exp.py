#!/usr/bin/env python3


"""Custom example of exponentially moving average.

Compare the bias corrected exp. moving average to the vanialla moving average to the difference it makes. Also, compare both with different discount rates beta.
"""


import numpy as np
import matplotlib.pyplot as plt


def plot_data(values_x: np.array, values_y: np.array):
    """Plots data into figure. """
    plt.figure()
    plt.plot(values_x, values_y, 'b+')
    plt.show()


def sim_data(base_func: callable, gaussian_noise_fac: int):
    """Simulates data from a base function by adding gaussian noise. """
    val_x = np.arange(1, 200, .5)
    val_y = np.array([base_func(x) + np.random.randn() * gaussian_noise_fac \
                      for x in val_x])
    return val_x, val_y

def compute_moving_avg(beta: float, values: np.array):
    """Computes vanilla moving average values. """
    ret = [0]
    for i in range(values):
        curr = (1-beta) * values[i]
        acc = beta * ret[i-1] + curr
        ret.append(acc)

    return ret


def compute_bias_corrected_moving_avg(beta: float, values:np.array):
    """Computes bias corrected moving average through division. """
    ret = [0]
    for i in range(values):
        curr = (1-beta) * values[i]
        acc = beta * ret[i-1] + curr
        ret.append(acc / (1-beta^i))

    return ret


if __name__ == '__main__':


