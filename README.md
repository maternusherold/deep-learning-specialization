# Deep Learning Spezializations by deeplearning.ai 

notes and programming exercises for the five specialization courses on [coursera](https://www.coursera.org/specializations/deep-learning).

## Current state

missing notes and exercises from course 1 on neural networks. Currently progressing course two on model optimization.

```text
.
├── Part2_Optimization_Hyperparam
│   ├── 01_ParamInitialization
│   ├── 02_Regularization
│   ├── 03_GradientChecking
│   ├── 04_OptimizationMethods
│   ├── 05_HyperparameterTuning
│   └── README.md
├── Part3_StructuringMLProjects
│   └── README.md
├── Part4_ConvolutionalNeuralNets
│   ├── 01_FoundationsCNNs
│   ├── 02_ClassicCNNs
│   ├── 03_ObjectLocalization
│   ├── 04_FaceRecognition_StyleTransfer
│   ├── readme_images
│   └── README.md
└── README.md

13 directories, 4 files
```

